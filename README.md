# Influence of Library Selection for Proteomics Experiments on the Accuracy of Error Rate Estimation

Poster presented at ASMS 2019 (Atlanta, GA).

See [Downloads](https://bitbucket.org/proteomesoftware/lib-fdr-2019/downloads/) to download the full poster.

For more information, see [http://www.proteomesoftware.com/dia](http://www.proteomesoftware.com/dia), or contact [Seth Just](mailto:seth.just@proteomesoftware.com).

## Copyright and License

Derived from "Jacobs Landscape Poster LaTeX Template"

For more information see [http://www.latextemplates.com/template/jacobs-landscape-poster](http://www.latextemplates.com/template/jacobs-landscape-poster)

Based on files from [http://www.latextemplates.com/templates/conference_posters/1/conference_poster_1.zip](http://www.latextemplates.com/templates/conference_posters/1/conference_poster_1.zip)

Template and poster contents (text and images) are licensed [CC Attribution-NC-SA](http://creativecommons.org/licenses/by-nc-sa/3.0/).

Code and other content in this repository is copyright Proteome Software Inc (2019), all rights reserved.
