%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Jacobs Landscape Poster
% LaTeX Template
% Version 1.1 (14/06/14)
%
% Created by:
% Computational Physics and Biophysics Group, Jacobs University
% https://teamwork.jacobs-university.de:8443/confluence/display/CoPandBiG/LaTeX+Poster
% 
% Further modified by:
% Nathaniel Johnston (nathaniel@njohnston.ca)
% Seth Just (Proteome Software Inc; seth.just@proteomesoftware.com)
%
% This template has been downloaded from:
% http://www.LaTeXTemplates.com
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
% See README.md for more license information.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass[final]{beamer}

\usepackage[utf8]{inputenc}

\usepackage[scale=1.10]{beamerposter} % Use the beamerposter package for laying out the poster

\usetheme{confposter} % Use the confposter theme supplied with this template

\definecolor{psblue}{cmyk}{0.75,0.55,0.21,0.03}
\definecolor{dpsblue}{cmyk}{0.82,0.66,0.36,0.18}

\setbeamercolor{block title}{fg=black,bg=white} % Colors of the block titles
\setbeamercolor{block body}{fg=black,bg=white} % Colors of the body of blocks
\setbeamercolor{block alerted title}{fg=white,bg=psblue} % Colors of the highlighted block titles
\setbeamercolor{block alerted body}{fg=black,bg=dblue!10} % Colors of the body of highlighted blocks
% Many more colors are available for use in beamerthemeconfposter.sty

\setbeamercolor{title in headline}{fg=white,bg=psblue}
 
\setbeamercolor{item}{fg=psblue}
\setbeamercolor{item projected}{fg=white,bg=psblue}

%-----------------------------------------------------------
% Define the column widths and overall poster size
% To set effective sepwid, onecolwid and twocolwid values, first choose how many columns you want and how much separation you want between columns
% In this template, the separation width chosen is 0.024 of the paper width and a 4-column layout
% onecolwid should therefore be (1-(# of columns+1)*sepwid)/# of columns e.g. (1-(4+1)*0.024)/4 = 0.22
% Set twocolwid to be (2*onecolwid)+sepwid = 0.464
% Set threecolwid to be (3*onecolwid)+2*sepwid = 0.708

\newlength{\sepwid}
\newlength{\onecolwid}
\newlength{\twocolwid}
\newlength{\threecolwid}
\setlength{\paperwidth}{48in} % A0 width: 46.8in
\setlength{\paperheight}{36in} % A0 height: 33.1in
\setlength{\sepwid}{0.024\paperwidth} % Separation width (white space) between columns -- 1.152"
\setlength{\onecolwid}{0.22\paperwidth} % Width of one column -- 10.56" (half a col is 5.28")
\setlength{\twocolwid}{0.464\paperwidth} % Width of two columns
\setlength{\threecolwid}{0.708\paperwidth} % Width of three columns
\setlength{\topmargin}{-0.5in} % Reduce the top margin size


\newlength{\figskip}
\setlength{\figskip}{3ex}
%-----------------------------------------------------------

\usepackage{graphicx}  % Required for including images

\usepackage{microtype} % Fancy adjustments at column edges

%\usepackage{lipsum} % Filler text

% These two packages together make captions use tighter line spacing.
\usepackage[labelformat=simple,justification=justified]{caption}
\usepackage[singlespacing]{setspace}

%\setbeamerfont{block body}{series=\ssffamily}

% package for fancy citations
% ---------------------------
\usepackage{natbib}

\usepackage[capitalise]{cleveref}

% set citation style to be superscript numbers
\bibpunct{}{}{,}{s}{}{,}

% fix an issue with natbib and beamer
\def\newblock{\hskip .11em plus .33em minus .07em}
% ---------------------------

\lefthyphenmin4
\righthyphenmin4
\hyphenation{spectro-metry phos-pho-rylation phos-pho-peptides nor-maliz-ation sta-tis-ti-cal}

\usepackage{xspace}
\newcommand{\piz}{\ensuremath{\pi_0}\xspace}

%----------------------------------------------------------------------------------------
%	TITLE SECTION 
%----------------------------------------------------------------------------------------

\title{Influence of Library Selection for Proteomics Experiments\\on the Accuracy of Error Rate Estimation} % Poster title

\newcommand{\upnum}[1]{\ensuremath{^\text{#1}}}
\newcommand{\upone}{\upnum{1}}
\newcommand{\uptwo}{\upnum{2}}
\newcommand{\upthree}{\upnum{3}}

\author{\underline{Seth Just}\upone, Caleb Emmons\upone, Jacob Lippincott\upone, Susan Ludwigsen\upone,\\Susan T. Weintraub\uptwo\ and Brian Searle\upnum{1,3}} % Author(s)

\institute{\upone\emph{Proteome Software Inc., Portland OR} \hspace{2ex} \uptwo\emph{University of Texas Health Science Center, San Antonio TX} \\ \upthree\emph{Institute for Systems Biology, Seattle WA}} % Institution(s)

%----------------------------------------------------------------------------------------

\begin{document}

\addtobeamertemplate{block end}{}{\vspace*{0ex}} % White space under blocks
\addtobeamertemplate{block alerted end}{}{\vspace*{1ex}} % White space under highlighted (alert) blocks

\setlength{\belowcaptionskip}{0ex} % White space under figures
\setlength{\belowdisplayshortskip}{0ex} % White space under equations

% From https://tex.stackexchange.com/a/160326
% Adds image to top corner of poster
% Adapted to put images in both corners -- SCJ 2016
\addtobeamertemplate{headline}{} 
{
  \begin{tikzpicture}[remember picture,overlay] 
  \node [shift={(\sepwid+0.1*\onecolwid,-5in)}] at (current page.north west) [anchor=south west] {\includegraphics[width=0.65\onecolwid]{img/scaffold_proteome_software_new_logo_2019_solid_white}};
  \end{tikzpicture} 
  
%  \begin{tikzpicture}[remember picture,overlay] 
%  \node [shift={(0.5\onecolwid+\sepwid,-8cm)}] at (current page.north west) {\includegraphics[height=12cm,trim=0 0 10.8cm 0,clip]{img/ps_logo_apparel_wht-trans}};
%  \end{tikzpicture} 
}
% End of code from tex.stackexchange.com

\begin{frame}[t] % The whole poster is enclosed in one beamer frame

\vspace{-5ex}

\begin{columns}[t] % The whole poster consists of three major columns, the second of which is split into two columns twice - the [t] option aligns each column's content to the top

\begin{column}{\sepwid}\end{column} % Empty spacer column

\begin{column}{\onecolwid} % The first column

%----------------------------------------------------------------------------------------
%	INTRODUCTION
%----------------------------------------------------------------------------------------

\begin{block}{Introduction}
	Proteomics experiments rely on statistical estimation and control of false discovery rates (FDR). These estimates vary dramatically depending on how target peptides are selected,\citep{bruderer17} either from spectrum/chromatogram libraries, or from FASTA databases. This selection considers a balance between managing error rate and comprehensive detection, where highly-specific libraries may exclude real targets, while comprehensive libraries or FASTA searches can provide more identifications at the cost of requiring stricter error rate control.	The proportion of searched target entries that are not detectable (\piz) is a key measure in evaluating this balance (\cref{fig:pi0-fig}). 

\begin{figure}
\includegraphics[width=0.8\linewidth]{img/pi0-fig}
\caption{
	\label{fig:pi0-fig}
	(a) Score distributions for target (blue) and decoy (orange) peptides.
	(b) Scaling the decoy distribution by the estimated proportion of undetectable peptides (\piz) approximates the distribution of incorrect target IDs.
	(c) We expect more high scoring targets than decoys. At lower scores, where the number of true targets drops to 0, the ratio of targets to decoys (T/D) gives an estimate of \piz.
	(d) Adding false ``entrapment'' targets (dashed purple line) to the search increases \piz, making it easier to accurately estimate.
}
\end{figure}

\vspace{-1.5ex}

\end{block}

%----------------------------------------------------------------------------------------
%	METHODS
%----------------------------------------------------------------------------------------

\begin{block}{Methods}

\vspace{-2ex}
\begin{figure}
\includegraphics[width=\linewidth]{img/pooled-workflow}
\caption{
	\label{fig:workflow}
	For this work we searched three libraries: the general purpose pan-human library,\citep{rosen14} a sample-specific library, and that same sample-specific library with additional false ``entrapment'' targets.
}
\end{figure}

We analyzed two human cell lines in triplicate using staggered 12~Da (``wide'') windows from 400-1000~m/z on an Orbitrap Fusion Lumos.
Pooled lysate was analyzed with staggered 4~Da windows in six gas-phase fractionation (GPF) experiments each covering 100~m/z.
Samples were searched with EncyclopeDIA\citep{searle18} using targets selected from the pan-human spectrum library,\citep{rosen14} a sample-specific (SS) library (generated from the pool), or the sample-specific library with additional entrapment peptides with random sequences (\cref{fig:workflow}).
Results were processed with Percolator\citep{kall07} to estimate \piz and control peptide-level FDR~to~1\%.

\end{block}

%----------------------------------------------------------------------------------------

\end{column} % End of the first column

\begin{column}{\sepwid}\end{column} % Empty spacer column

\begin{column}{\twocolwid} % Begin a column which is two columns wide (column 2)

%----------------------------------------------------------------------------------------
%	RESULTS
%----------------------------------------------------------------------------------------

\begin{block}{Results}

\vspace{-2.5ex}

\begin{figure}
\includegraphics[width=\linewidth]{img/score-dists}
\vspace{-4ex}
\caption{
	\label{fig:score-dists}
	We grouped peptide IDs into bins of 1000 according to score.
	Within each bin, we computed the relative density of target, entrapment, and decoy peptides (left side),
	and the ratio of targets (and entrapment) peptides to decoys (right side)
	These ratios agreed well with Percolator's \piz estimates (dashed lines), but show how \piz is harder to estimate accurately when there are fewer low-scoring target peptides.
}
\end{figure}

\vspace{-3ex}

\begin{columns}[t,totalwidth=\twocolwid] % Split up the two columns wide column

\begin{column}{\onecolwid} % The first column within column 2 (column 2.1)
\justify % not sure why we need this; it should be default

\begin{figure}
\includegraphics[width=0.9\linewidth, trim=0 4mm 0 3mm, clip]{img/peptide-count-bars}
\caption{
	\label{fig:pep-counts}
	Sensitivity is reduced when searching wide-window samples (dashed arrow).
	The high peptide count for a sample-specific library suggested a problem with error rate estimation.
}
\end{figure}

\vspace{1ex}

Low \piz values can be beneficial by imposing a lower burden for FDR correction, but are both noisier to estimate (\cref{fig:score-dists}) and the effects of errors in that estimate are proportionally large (e.g. $2\%\pm1\%$ is a much larger error than $50\%\pm1\%$). 
We suspected poor estimation of FDR based on an unusually high number of peptide detections from our sample-specific library (\cref{fig:pep-counts}).
To monitor the potential breakdown of error rate estimates, we calculated an ``entrapment FDR'' using the additional entrapment peptides with labels hidden from Percolator (\cref{fig:entrapment-analysis}).
Normal decoy/target FDR estimation with low \piz values produced FDRs dramatically lower than expected, but we could recover valid FDRs by considering additional unlabeled decoys (\cref{fig:entrap-comp}).
	
%----------------------------------------------------------------------------------------

\end{column} % End of column 2.1

\begin{column}{\onecolwid} % The second column within column 2 (column 2.2)

\begin{figure}
\includegraphics[width=\linewidth]{img/entrapment-analysis}
\caption{
	\label{fig:entrapment-analysis}
	(a) Graphical schematic of normal decoy analysis without additional decoys.
	(b) Searching with the SS+entrapment library allows two orthogonal estimates of error rate which make use of additional decoys.
}
\end{figure}

\vspace{-2ex}

\begin{figure}
%\includegraphics[width=\linewidth, trim=0 0 0 2mm, clip]{img/entrap-comp}
\includegraphics[width=\linewidth, trim=0 0 0 4mm, clip]{img/entrap-comp-adjaxislabels}
\caption{
	\label{fig:entrap-comp}
	Comparison of entrapment FDR (purple/green in \mbox{\cref{fig:entrapment-analysis}(b)}) to either considering only the sample-specific library peptides (\mbox{\cref{fig:entrapment-analysis}(a)}), or the library plus additional decoys from the entrapment process (orange/blue in \mbox{\cref{fig:entrapment-analysis}(b)}).
	Left: full results; right: from 0-5\% FDR.
	Estimates with additional decoys agreed well with the entrapment-based FDRs created with labels hidden from Percolator, but normal target/decoy analysis of this search is anti-conservative: an estimated 1\% FDR corresponds to 20\% FDR when using additional decoys.
}
\end{figure}

%----------------------------------------------------------------------------------------

\end{column} % End of column 2.2

\end{columns} % End of the split of column 2 - any content after this will now take up 2 columns width

\end{block}

\end{column} % End of the second column

\begin{column}{\sepwid}\end{column} % Empty spacer column

\begin{column}{\onecolwid} % The third column

%----------------------------------------------------------------------------------------
%	CONCLUSION
%----------------------------------------------------------------------------------------

\begin{block}{Conclusions}

\begin{itemize}
  \item Sample-specific libraries can improve detection rates, but can also lead to breakdown of FDR estimation when having few incorrect target matches makes it difficult to estimate their distribution.
  \item While we demonstrate this with Percolator, it affects all target/decoy library-based search engines.
        Strict criteria for matches (e.g. narrow retention time ranges) and lack of cross-fold validation in mProphet-based search engines may exacerbate the problem.
  \item Poor error rate estimation adversely affects accuracy and consistency of results, in both identification and quantification (\cref{fig:navarro-scatter}).
  \item Adding known-false targets to sample-specific libraries can improve error rate estimation without strongly affecting sensitivity and allow independent verification of FDR estimates.
\end{itemize}

\begin{figure}
\includegraphics[width=\linewidth, trim=0 2mm 0 2mm, clip]{img/navarro-scatter}
\caption{
	\label{fig:navarro-scatter}
	Quantitative ratios for peptide identifications differing between two searches of two mixed-proteome samples with a known A/B ratio for each species.\citep{navarro16}
	Left: searched with a sample-specific library generated from DDA data; right: with the same library and additional entrapment targets.
	While the peptide count at $1\%$ FDR slightly decreased when searching the SS+entrapment library we see changes in about $4\%$ of peptide identifications.
	Results from the SS+entrapment library overall show improved quantitative reproducibility, especially at low intensities.
}
\end{figure}

\vspace{-2ex}

\end{block}

%----------------------------------------------------------------------------------------
%	REFERENCES
%----------------------------------------------------------------------------------------

\begin{block}{References}

%\nocite{*} % Insert publications even if they are not cited in the poster

\footnotesize{
\bibliographystyle{ama}
\bibliography{references}
}

\end{block}

%----------------------------------------------------------------------------------------
%	ACKNOWLEDGEMENTS
%----------------------------------------------------------------------------------------

%\setbeamercolor{block title}{fg=red,bg=white} % Change the block title color

%\begin{block}{Acknowledgements}
%
%\footnotesize{\rmfamily{
%Nam mollis tristique neque eu luctus. Suspendisse rutrum congue nisi sed convallis. Aenean id neque dolor. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
%}} \\
%
%\end{block}

%----------------------------------------------------------------------------------------
%	CONTACT INFORMATION
%----------------------------------------------------------------------------------------

%\setbeamercolor{block alerted title}{fg=black,bg=norange} % Change the alert block title colors
%\setbeamercolor{block alerted body}{fg=black,bg=white} % Change the alert block body colors

%\vspace{-2ex}

%\begin{alertblock}{Contact Information}
\begin{block}{Contact Information}

\begin{itemize}
\item Web: {\small \ttfamily \href{https://bitbucket.org/proteomesoftware/lib-fdr-2019}{bitbucket.org/proteomesoftware/lib-fdr-2019}}
\item Email: {\small \ttfamily \href{mailto:seth.just@proteomesoftware.com}{seth.just@proteomesoftware.com}}
\item Phone: +1 800 944 6027
\end{itemize}

\end{block}

\begin{tikzpicture}[remember picture,overlay] 
  \node [shift={(-0.85\sepwid,1cm)}] at (current page.south east) [anchor=south east] {\includegraphics[width=6cm]{img/by-nc-sa}};
\end{tikzpicture} 

%\begin{center}
%\begin{tabular}{ccc}
%\includegraphics[width=0.4\linewidth]{logo.png} & \hfill & \includegraphics[width=0.4\linewidth]{logo.png}
%\end{tabular}
%\end{center}

%----------------------------------------------------------------------------------------

\end{column} % End of the third column

\end{columns} % End of all the columns in the poster

\end{frame} % End of the enclosing frame

\end{document}


