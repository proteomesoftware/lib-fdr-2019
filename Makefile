SHELL := /bin/bash

LATEX = pdflatex --interaction=nonstopmode
REDIR = 1>/dev/null 2>/dev/null
BIBTEX = bibtex
RM = rm -f

RERUN = "(There were undefined references|Rerun to get ((cross-references|the bars) right|citations correct))"
RERUNBIB = "No file.*\.bbl|Citation.*undefined"

MAIN = main

BIB = references.bib

IMGS = img/by-nc-sa.pdf img/scaffold_proteome_software_new_logo_2019_solid_white.pdf img/pi0-fig.pdf img/pooled-workflow.pdf img/entrapment-analysis.pdf img/score-dists.pdf img/peptide-count-bars.pdf img/entrap-comp.pdf img/navarro-scatter.pdf img/entrap-comp-adjaxislabels.pdf

#.SILENT:

.PHONY: all clean reallyclean hgclean

# Specify what we want built by default. We have to specify the main file, even though it's
# a prereq of the compressed file because make automatically deletes intermediate files.
all: $(MAIN).pdf peptide-stats.nbconvert.pdf navarro-quant.nbconvert.pdf #$(MAIN)-compressed.pdf $(MAIN)-compressed-printer.pdf

#####################
# BEGIN IMAGE RULES #
#####################
img/score-dists.pdf img/peptide-count-bars.pdf img/entrap-comp.pdf: peptide-stats.nbconvert.ipynb

img/entrap-comp-adjaxislabels.pdf: img/entrap-comp.pdf

img/navarro-scatter.pdf: navarro-quant.nbconvert.ipynb

%.nbconvert.ipynb: %.ipynb
	. ~/python/jupyterlab-venv/bin/activate && time jupyter-nbconvert --to notebook --execute $<

img/%.pdf: img/%.svg
	inkscape --export-area-page --export-pdf=$@ $<
####################
# END IMAGE RULES  #
####################

%-compressed.pdf: %.pdf
	gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dNOPAUSE -dQUIET -dBATCH -dPDFSETTINGS=/default -sOutputFile=$@ $<

%-compressed-printer.pdf: %.pdf
	gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dNOPAUSE -dQUIET -dBATCH -dPDFSETTINGS=/printer -sOutputFile=$@ $<

%.nbconvert.pdf: %.nbconvert.ipynb
	jupyter-nbconvert --to pdf --output $* $<

%.pdf: %.tex %.aux %.bbl %.blg $(IMGS)
	$(LATEX) $< #$(REDIR)
	egrep $(RERUN) $*.log && $(LATEX) $< $(REDIR); true
	egrep $(RERUN) $*.log && $(LATEX) $< $(REDIR); true
	echo "*** Errors for $< ***"; true
	egrep -i "((Reference|Citation).*undefined|Unaddressed TODO)" $*.log ; true

%.aux: %.tex
	$(LATEX) $< $(REDIR); true

%.bbl %.blg: %.aux $(BIB)
# Dependence on the aux makes sure we rerun when changed, but will fire too often
	$(BIBTEX) $* $(REDIR); true
	$(LATEX) $* $(REDIR); true
	egrep -c $(RERUNBIB) $*.log $(REDIR) && ($(BIBTEX) $* $(REDIR);$(LATEX) $* $(REDIR)) ; true

clean: 
	$(RM) *.log *.aux *.toc *.tof *.tog *.bbl *.blg *.pdfsync *.d *.dvi *.out *.thm vc.tex *.nav *.snm $(NBIMGS)

reallyclean:
	$(RM) *.pdf

hgclean: 
	hg status -u --no-status --print0 | xargs -0 rm
